import React, { useState } from "react";

import { useMediaQuery } from "react-responsive";
import { useNavigate } from "react-router";

import { predavaci } from "../constants/predavaci";
import { predavanja } from "../constants/predavanja";
import { konferencija } from "../constants/konferencija";
import { topics } from "../constants/topics";

import { ReactComponent as LoginIlustration } from "../assets/svg/login-ilustration.svg";
import { ReactComponent as RegisterIlustration } from "../assets/svg/register-ilustration.svg";
import { ReactComponent as LocationIcon } from "../assets/svg/location.svg";
import { ReactComponent as LogoIcon } from "../assets/svg/icon.svg";
import { ReactComponent as EducatorsIlustration } from "../assets/images/educators-decoration.svg";
import { Splide, SplideSlide } from "@splidejs/react-splide";
import Paragraph from "../components/Paragraph";
import Button from "../components/Button";
import Tag from "../components/Tag";
import Benefits from "../components/Benefits";
import countries from "i18n-iso-countries";

countries.registerLocale(require("i18n-iso-countries/langs/hr.json"));

const Home = () => {
  const [activeTopic, setActiveTopic] = useState(0);
  const isBigScreen = useMediaQuery({ query: "(max-width: 1540px)" });
  const isMediumScreen = useMediaQuery({ query: "(max-width: 1400px)" });
  const isSmallScreen = useMediaQuery({ query: "(max-width: 1000px)" });
  const isTablet = useMediaQuery({ query: "(max-width: 900px)" });
  const isMobile = useMediaQuery({ query: "(max-width: 600px)" });
  const navigate = useNavigate();

  const getCarouselPadding = () => {
    if (isMobile) {
      return 30;
    }

    if (isTablet) {
      return 60;
    }

    return 120;
  };

  return (
    <>
      <section className="banner">
        <div className="banner__content">
          <h1>
            NeoUm<br></br>
            <span>Konferencija</span>
            <br></br>2023
          </h1>
          <h2>Osvestimo ono što znamo u našim srcima da je istinito!</h2>
          <div className="banner__cta">
            <div className="banner__information">
              <Paragraph variant={"title"}>11. Novembar</Paragraph>
              <Paragraph variant={"subtitle"} fontWeight={300}>
                Madlenianum, Beograd
              </Paragraph>
            </div>
            <Button variant={"primary"} onClick={() => navigate("/register")}>
              Ulaznice
            </Button>
          </div>
        </div>
      </section>
      <section className="topics">
        <h1 className="topics__title">Evo šta ćete naučiti na konferenciji:</h1>
        <div className="topics__content">
          {!isBigScreen && (
            <div className="topics__types">
              {topics.map(({ name }, index) => (
                <Tag
                  isActive={activeTopic === index}
                  onClick={() => setActiveTopic(index)}
                  key={name}
                >
                  <h2>{name}</h2>
                </Tag>
              ))}
            </div>
          )}
          {isBigScreen && (
            <div className="topics__header">
              {!isSmallScreen && (
                <div className="topics__types">
                  {topics.map(({ name }, index) => (
                    <Tag
                      key={name}
                      isActive={activeTopic === index}
                      onClick={() => setActiveTopic(index)}
                    >
                      <h2>{name}</h2>
                    </Tag>
                  ))}
                </div>
              )}
              {isSmallScreen && (
                <div className="topics__carousel">
                  <Splide
                    options={{
                      autoWidth: true,
                      gap: 12,
                      pagination: false,
                      arrows: true,
                      padding: getCarouselPadding(),
                      autoplay: true,
                    }}
                    aria-label="Topics Carousel"
                  >
                    {topics.map(({ name }, index) => (
                      <SplideSlide key={name}>
                        <Tag
                          isActive={activeTopic === index}
                          onClick={() => setActiveTopic(index)}
                        >
                          <h2>{name}</h2>
                        </Tag>
                      </SplideSlide>
                    ))}
                  </Splide>
                </div>
              )}
              <div className="topics__svg">
                {topics[activeTopic]?.ilustration}
              </div>
            </div>
          )}
          <div className="topics__type">
            {!isBigScreen && (
              <div className="topics__svg">
                {topics[activeTopic]?.ilustration}
              </div>
            )}
            <div className="topics__description">
              <>
                <h2>{topics[activeTopic]?.title}</h2>
                <div className="topics__details">
                  {topics[activeTopic]?.description}
                </div>
              </>

              <div className="topics__cta">
                <Button
                  variant={"primary"}
                  onClick={() => navigate("/register")}
                >
                  Ulaznice
                </Button>
                <div>
                  <Paragraph variant={"title"} fontWeight={500}>
                    NeoUm Konferencija
                  </Paragraph>
                  <Paragraph variant={"subtitle"} fontWeight={300}>
                    11. Novembar 2023, Beograd
                  </Paragraph>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className="educators" id="edukatori">
        <div className="educators__header">
          <h1 className="educators__title">Predavači</h1>
          <Paragraph variant={"title"} fontWeight={500}>
            Neki od najmoćnijih lidera, na našim prostorima, okupiće se da
            podele svoja najdublja razmišljanja i smernice za korak napred u
            ovom ključnom trenutku.
          </Paragraph>
        </div>

        {predavaci.map(({ image, name, profession, to }) => {
          return (
            <div
              onClick={() => {
                navigate(to);
              }}
              key={name}
              className="educators__item"
            >
              <img src={image} alt={`${name} - ${profession}`} />
              <Paragraph variant="title" fontWeight={500}>
                {name}
              </Paragraph>
              <Paragraph variant="p2" fontWeight={300}>
                {profession}
              </Paragraph>
            </div>
          );
        })}
      </section>
      <section className="goal">
        <div className="goal__svg">
          <LoginIlustration />
        </div>
        <div className="goal__content">
          <h1 className="goal__title">
            Sada je vreme za transformaciju na svim nivoima.
          </h1>
          <Paragraph variant={"title"} fontWeight={500}>
            Do kraja konferencije, osećaćete se spremnim da odgovorite na
            hitnost ovoga trenutka time što ćete pristupiti neograničenom
            potencijalu koji se nesumnjivo nalazi u vama i odgovoriti na zov
            vašeg srca da konačno krenete ka vašoj sudbini.
          </Paragraph>
          <div className="goal__cta">
            <Button variant={"primary"} onClick={() => navigate("/register")}>
              Ulaznice
            </Button>
            <div>
              <Paragraph variant={"title"} fontWeight={500}>
                NeoUm Konferencija
              </Paragraph>
              <Paragraph variant={"subtitle"} fontWeight={300}>
                11. Novembar 2023, Beograd
              </Paragraph>
            </div>
          </div>
        </div>
      </section>
      <section className="program" id="program">
        {isMediumScreen && (
          <EducatorsIlustration className="program__background" />
        )}
        <div className="program__content">
          <div className="program__header">
            <h1>Program</h1>
            <h2>Subota 11. Novembar 2023.</h2>
            <Paragraph variant={"title"} fontWeight={500}>
              Na konferenciji će nastupiti šest edukatora koji će samostalno
              podeliti svoje znanje i iskustvo koje će vam omogućiti da
              otključate neograničeni potencijal koji se nalazi u vama, kao i
              dva panela edukatora (po pet edukatora) koji će diskutovati o
              pitanjima na koje tražite odgovore.
            </Paragraph>
          </div>
          <div className="program__plan">
            {predavanja.map(({ time, title, subtitle, image }) => {
              return (
                <div key={time}>
                  {!isMobile && (
                    <div className="program__item" key={time}>
                      <Paragraph variant={"p2"} fontWeight={500}>
                        {time}
                      </Paragraph>
                      <div>
                        <Paragraph variant={"title"} fontWeight={500}>
                          {title}
                        </Paragraph>
                        <Paragraph variant={"subtitle"} fontWeight={300}>
                          {subtitle}
                        </Paragraph>
                      </div>
                      {image && (
                        <img
                          src={image}
                          alt={`${time} - ${title} - ${subtitle}`}
                        />
                      )}
                    </div>
                  )}
                  {isMobile && (
                    <div className="program__item" key={time}>
                      <div>
                        <Paragraph variant={"p2"} fontWeight={500}>
                          {time}
                        </Paragraph>
                        <Paragraph variant={"title"} fontWeight={500}>
                          {title}
                        </Paragraph>
                        <Paragraph variant={"subtitle"} fontWeight={300}>
                          {subtitle}
                        </Paragraph>
                      </div>
                      {image && (
                        <img
                          src={image}
                          alt={`${time} - ${title} - ${subtitle}`}
                        />
                      )}
                    </div>
                  )}
                </div>
              );
            })}
          </div>
          {!isMediumScreen && <LogoIcon className="program__background" />}
        </div>
        <div className="program__carousel">
          <Splide
            options={{
              perMove: 1,
              perPage: 1,
              autoWidth: true,
              gap: 24,
              pagination: false,
              arrows: false,
              padding: getCarouselPadding(),
              autoplay: true,
            }}
            aria-label="Conference Location Images"
          >
            {konferencija.map(({ image, alt }) => {
              return (
                <SplideSlide key={alt}>
                  <img src={image} alt={alt} />
                </SplideSlide>
              );
            })}
          </Splide>
        </div>
        <div
          className="program__location"
          onClick={() =>
            window.open(
              "https://moovitapp.com/index/en/public_transit-Madlenianum_Madlenianum_Opera_and_Theatre-Belgrade_Beograd-site_33873692-3304",
              "_blank"
            )
          }
        >
          <h1>Madlenianum</h1>
          <div>
            <div>
              <LocationIcon />
            </div>
            <h2>
              Glavna 32 <br></br> Zemun, Beograd
            </h2>
          </div>
          <Paragraph variant={"p2"} fontWeight={300}>
            Konferencija će se održati u velelepnoj dvorani Madlenianum Teatra.
          </Paragraph>
        </div>
      </section>
      <section className="tickets" id="ulaznice">
        <div className="tickets__svg">
          <RegisterIlustration />
        </div>
        <div className="tickets__content">
          <div className="tickets__header">
            <h1>Ulaznice</h1>
            <div className="tickets__seats">
              <Paragraph variant={"p3"}>Ograničen broj mesta</Paragraph>
              <h2>450</h2>
            </div>
          </div>
          <div className="tickets__privileges">
            <Paragraph variant={"p2"} fontWeight={300}>
              Ulaznica obuhvata:
            </Paragraph>
            <Benefits />
            <div className="tickets__pricing">
              <Paragraph variant={"p2"} fontWeight={300}>
                Cena ulaznice
              </Paragraph>
              <div className="tickets__price">
                <h1>44€</h1>
              </div>
            </div>
            <Button variant={"primary"} onClick={() => navigate("/register")}>
              Kupi ulaznicu
            </Button>
          </div>
        </div>
      </section>
    </>
  );
};

export default Home;
