import { ReactComponent as PurchaseSuccessIllustration } from "../assets/ilustration/success-ilustration.svg";
import Paragraph from "../components/Paragraph";
import Button from "../components/Button";
import { useNavigate } from "react-router";

const PurchaseSuccess = () => {
  const navigate = useNavigate();

  return (
    <div className="success">
      <div className="success_page">
        <PurchaseSuccessIllustration />
        <div className="success_content">
          <h1>Kupovina uspešno završena</h1>
          <div className="success_text success_text--modified">
            <Paragraph variant="p2" fontWeight={500}>
              Sačuvajte email koji budete dobili kao potvrdu kupovine kako biste
              mogli da prisustvujete događaju.
            </Paragraph>
            <Paragraph variant="p2" fontWeight={500}>
              Molimo vas da proverite spam folder u slučaju da vam ne stigne
              email u glavnu poštu.
            </Paragraph>
            <Paragraph variant="p2" fontWeight={300}>
              Ukoliko imate nekih pitanja ili nejasnoća možete nam se obratiti
              na email{" "}
              <a
                className="success_link"
                href="mailto:neoumkonferencija@gmail.com"
              >
                neoumkonferencija@gmail.com
              </a>
            </Paragraph>
          </div>
          <div className="button_wrap">
            <Button onClick={() => navigate("/")} variant="primary">
              Početna
            </Button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default PurchaseSuccess;
