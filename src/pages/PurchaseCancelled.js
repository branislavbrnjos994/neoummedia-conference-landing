import { ReactComponent as PurchaseCancelledIllustration } from "../assets/ilustration/canceled-ilustration.svg";
import Paragraph from "../components/Paragraph";
import Button from "../components/Button";
import { useNavigate } from "react-router";

const PurchaseSuccess = () => {
  const navigate = useNavigate();

  return (
    <div className="success">
      <div className="success_page">
        <PurchaseCancelledIllustration />
        <div className="success_content">
          <h1>Kupovina prekinuta</h1>
          <div className="success_text">
            <Paragraph variant="p2" fontWeight={500}>
              Ako ste greškom prekinuli kupovinu, vratite se na početnu, kako
              biste pokušali ponovo.
            </Paragraph>
            <Paragraph variant="p2" fontWeight={300}>
              Ukoliko imate nekih pitanja ili nejasnoća možete nam se obratiti
              na email{" "}
              <a
                className="success_link"
                href="mailto:neoumkonferencija@gmail.com"
              >
                neoumkonferencija@gmail.com
              </a>
            </Paragraph>
          </div>
          <div className="button_wrap">
            <Button onClick={() => navigate("/")} variant="primary">
              Početna
            </Button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default PurchaseSuccess;
