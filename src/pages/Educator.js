import React, { useEffect, useState } from "react";
import Paragraph from "../components/Paragraph";
import Button from "../components/Button";
import { ReactComponent as SpeakerDecoration } from "../assets/images/speaker-decoration.svg";
import { useLocation, useNavigate } from "react-router";
import { predavaci } from "../constants/predavaci";

const Educator = () => {
  const { pathname } = useLocation();
  const [educator, setEducator] = useState(0);

  useEffect(() => {
    const parts = pathname.split("/");
    const lastPart = parts[parts.length - 1];
    setEducator(lastPart);
  }, [pathname]);

  const navigate = useNavigate()

  return (
    <div className="educator">
      <div className="educator__content">
        <h1 className="educator__title">{predavaci[educator]?.name}</h1>
        <div className="educator__conference">
          <Paragraph
            className="educator__subtitle"
            fontWeight={500}
            variant="p2"
          >
            Predavanja:
          </Paragraph>
          <h2 className="educator__time">{predavaci[educator]?.time}</h2>
        </div>
        <div className="educator__description">
          <Paragraph variant="title">
            {predavaci[educator]?.description}
          </Paragraph>
        </div>
        <div className="educator__cta">
          <Button variant={"primary"} onClick={() => navigate('/register')}>Ulaznice</Button>
          <div>
            <Paragraph variant="title" fontWeight={500}>
              NeoUm Konferencija
            </Paragraph>
            <Paragraph variant="subtitle" fontWeight={300}>
              11. Novembar 2023, Beograd
            </Paragraph>
          </div>
        </div>
      </div>
      <div className="educator__background">
        <img
          src={predavaci[educator]?.imageBig}
          alt={predavaci[educator]?.name}
        />
      </div>

      <SpeakerDecoration />
    </div>
  );
};

Educator.propTypes = {};

export default Educator;
