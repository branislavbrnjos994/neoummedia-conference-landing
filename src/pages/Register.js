import React, { useState } from "react";
import { ReactComponent as ProfileIlustration } from "../assets/ilustration/profile-ilustration.svg";

import { ReactComponent as UserIcon } from "../assets/svg/user.svg";
import { ReactComponent as PlayIcon } from "../assets/svg/play.svg";

import * as yup from "yup";
import { useMediaQuery } from "react-responsive";
import Benefits from "../components/Benefits";
import LegalEntityForm from "../components/LegalEntityForm";
import NormalEntityForm from "../components/NormalEntityForm";
import MoreTickets from "../components/MoreTickets";
import { POST } from "../services/requests";
import { useMutation } from "@tanstack/react-query";
import { toast } from "react-toastify";
import { useCountries } from "../hooks/useCountries";
import { convertCountryToIso3Code } from "../utils/convertCountry";
import Tabs from "../components/Tabs";
import Checkbox from "../components/Checkbox";

const PolicyCheckboxes = (isLiveStream) => {
  return (
    <div className="register__policies">
      <Checkbox
        name="liveStreamPolicy"
        id="liveStreamPolicy"
        label={
          isLiveStream
            ? "Saglasan sam “Ako kupite ulaznice za uživo prisustvo, napomena da live streaming nije uključen.”"
            : "Ako kupite pristup za live streaming, imajte na umu da to ne uključuje ulaznice za uživo prisustvo. U slučaju da ne možete ispratiti konferenciju uživo putem live stream-a, naknadni snimak neće biti poslat."
        }
      />
      <Checkbox
        name="refundPolicy"
        id="liveStreamPolicy"
        label={
          "Potvrdite da ste upoznati sa našom politikom bez povrata sredstava."
        }
      />
    </div>
  );
};

const Register = () => {
  const [type, setType] = useState("normal_entity");
  const [isLiveStream, setIsLiveStream] = useState(false);
  const isMediumScreen = useMediaQuery({ query: "(max-width: 1400px)" });
  const [extraTicketsCount, setExtraTicketsCount] = useState(0);
  const { countries } = useCountries();
  const { mutate, isLoading } = useMutation((payload) =>
    POST("/conference/register-and-request-checkout", payload)
  );

  const handleSubmit = (values) => {
    const {
      bearerFullName,
      bearerEmail,
      country,
      city,
      address,
      taxId,
      firm,
      question,
    } = values;

    const countryList = Object.values(countries);

    const iso3Code = convertCountryToIso3Code(country);

    if (
      !countryList.some((item) => item.toLowerCase() === country.toLowerCase())
    ) {
      toast("Država nije ispravna. Molimo vas unesite je ispravno.");
      return;
    }

    const legalEntityPayload =
      type === "legal_entity"
        ? {
            taxId,
            firm,
          }
        : {};

    const otherMembersData = [];

    for (let i = 1; i <= extraTicketsCount; i++) {
      const fullName = values[`name${i}`];
      const email = values[`email${i}`];

      if (fullName && email) {
        otherMembersData.push({ fullName, email });
      }
    }

    const payload = {
      bearerFullName,
      bearerEmail,
      country: iso3Code || country,
      city,
      address,
      otherMembersCount: otherMembersData.length,
      otherMembersData,
      question,
      ...legalEntityPayload,
      type: type === "normal_entity" ? "Private" : "Legal",
      isLiveStream: isLiveStream,
    };

    mutate(payload, {
      onSuccess: ({ url }) => {
        window.location.href = url;
      },
      onError: (error) => {
        toast("Zahtev nije uspeo, molimo vas pokušajte ponovo.");
      },
    });
  };

  const mergedValidationSchema =
    type === "normal_entity"
      ? yup.object().shape({
          bearerFullName: yup
            .string()
            .required("Ime i prezime je obavezno polje."),
          bearerEmail: yup
            .string()
            .email("Email mora biti ispravnog formata.")
            .required("Email je obavezno polje."),
          country: yup.string().required("Drzava je obavezno polje."),
          city: yup.string().required("Grad je obavezno polje."),
          liveStreamPolicy: yup.bool().oneOf([true], "Must Agree"),
          refundPolicy: yup.bool().oneOf([true], "Must Agree"),
          ...Array(extraTicketsCount)
            .fill()
            .reduce((schema, _, index) => {
              schema[`name${index + 1}`] = yup
                .string()
                .required("Ime i prezime je obavezno polje.");
              schema[`email${index + 1}`] = yup
                .string()
                .email("Email mora biti ispravnog formata.")
                .required("Email je obavezno polje.");
              return schema;
            }, {}),
        })
      : type === "legal_entity"
      ? yup.object().shape({
          firm: yup.string().required("Naziv firme je obavezno polje."),
          taxId: yup.string().required("PIB/OIB firme je obavezno polje."),
          bearerFullName: yup
            .string()
            .required("Ime i prezime je obavezno polje."),
          bearerEmail: yup
            .string()
            .email("Email mora biti ispravnog formata.")
            .required("Email je obavezno polje."),
          country: yup.string().required("Drzava je obavezno polje."),
          city: yup.string().required("Grad je obavezno polje."),
          address: yup.string().required("Adresa i broj je obavezno polje."),
          liveStreamPolicy: yup.bool().oneOf([true], "Must Agree"),
          refundPolicy: yup.bool().oneOf([true], "Must Agree"),
          ...Array(extraTicketsCount)
            .fill()
            .reduce((schema, _, index) => {
              schema[`name${index + 1}`] = yup
                .string()
                .required("Ime i prezime je obavezno polje.");
              schema[`email${index + 1}`] = yup
                .string()
                .email("Email mora biti ispravnog formata.")
                .required("Email je obavezno polje.");
              return schema;
            }, {}),
        })
      : {};

  return (
    <div className="register">
      <div className="register__form-container">
        <h1 className="register__title">Prijava</h1>
        <Tabs
          tabs={[
            {
              label: "Uživo pristustvo",
              value: false,
              onClick: () => setIsLiveStream(false),
              icon: <UserIcon />,
            },
            {
              label: "Live stream",
              value: true,
              onClick: () => setIsLiveStream(true),
              icon: <PlayIcon />,
            },
          ]}
          currentValue={isLiveStream}
          variant={"filled"}
        />
        <Tabs
          tabs={[
            {
              label: "Fizičko lice",
              value: "normal_entity",
              onClick: () => setType("normal_entity"),
            },
            {
              label: "Pravno lice",
              value: "legal_entity",
              onClick: () => setType("legal_entity"),
            },
          ]}
          currentValue={type}
          variant={"outlined"}
        />

        {type === "normal_entity" && (
          <NormalEntityForm
            extraTickets={
              <MoreTickets
                setExtraTicketsCount={setExtraTicketsCount}
                extraTicketsCount={extraTicketsCount}
              />
            }
            policies={<PolicyCheckboxes isLiveStream={isLiveStream} />}
            extraTicketsCount={extraTicketsCount}
            handleSubmit={handleSubmit}
            schema={mergedValidationSchema}
            isLoading={isLoading}
          />
        )}

        {type === "legal_entity" && (
          <LegalEntityForm
            extraTickets={
              <MoreTickets
                setExtraTicketsCount={setExtraTicketsCount}
                extraTicketsCount={extraTicketsCount}
              />
            }
            policies={<PolicyCheckboxes isLiveStream={isLiveStream} />}
            extraTicketsCount={extraTicketsCount}
            handleSubmit={handleSubmit}
            schema={mergedValidationSchema}
            isLoading={isLoading}
          />
        )}
      </div>
      {!isMediumScreen && (
        <div className="register__tickets">
          <div className="register__image">
            <ProfileIlustration />
          </div>
          <div className="register__includes">
            <div className="register__badge">
              <h2>Ulaznica obuhvata</h2>
            </div>
            <Benefits />
          </div>
          <div className="register__badge">
            Cena {extraTicketsCount === 0 ? "ulaznice" : "ulaznica"}{" "}
            <span>{(extraTicketsCount + 1) * 44}€</span>
          </div>
        </div>
      )}
    </div>
  );
};

Register.propTypes = {};

export default Register;
