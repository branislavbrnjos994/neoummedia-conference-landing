import React from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import "./App.scss";
import Home from "./pages/Home";
import Register from "./pages/Register";
import Header from "./components/Header";
import Footer from "./components/Footer";
import Educator from "./pages/Educator";
import PurchaseSuccess from "./pages/PurchaseSuccess";
import PurchaseCancelled from "./pages/PurchaseCancelled";
import "@splidejs/react-splide/css";
import { QueryClient, QueryClientProvider } from "@tanstack/react-query";
import "react-toastify/dist/ReactToastify.css";
import { ToastContainer } from "react-toastify";
import { loadStripe } from "@stripe/stripe-js";

const App = () => {
  const queryClient = new QueryClient();
  const stripe = loadStripe(process.env.REACT_APP_STRIPE_PUBLIC_KEY);

  return (
    <div className="App">
      <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar={true}
        newestOnTop={false}
        draggable
        theme="light"
      />
      <BrowserRouter>
        <QueryClientProvider client={queryClient}>
          <Header />
          <main>
            <Routes>
              <Route path="/" element={<Home />} />
              <Route path="/register" element={<Register />} />
              <Route path="/educator/:educator" element={<Educator />} />
              <Route path="*" element={<Home />} />
              <Route path="/purchase/success" element={<PurchaseSuccess />} />
              <Route path="/purchase/cancel" element={<PurchaseCancelled />} />
            </Routes>
          </main>
          <Footer />
        </QueryClientProvider>
      </BrowserRouter>
    </div>
  );
};

export default App;
