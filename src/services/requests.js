import api from "./instance";

const makeRequest = async (method, endpoint, dataOrParams = {}) => {
  try {
    const response = await api({
      method,
      url: endpoint,
      ...(method === "get" ? { params: dataOrParams } : { data: dataOrParams }),
    });
    return response.data;
  } catch (error) {
    throw error;
  }
};

export const GET = async (endpoint, params = {}) =>
  makeRequest("get", endpoint, params);

export const POST = async (endpoint, data) =>
  makeRequest("post", endpoint, data);

export const PUT = async (endpoint, data) => makeRequest("put", endpoint, data);
