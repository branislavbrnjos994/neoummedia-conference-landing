import axios from "axios";

const api = axios.create({
  baseURL: process.env.REACT_APP_BASE_API_URL,
  timeout: 60000,
  headers: {
    "Content-Type": "application/json",
  },
});

api.interceptors.response.use(
  (response) => {
    return response;
  },
  (error) => {
    console.error("API ERROR:", error);
    return Promise.reject(error);
  }
);

export default api;
