import countries from "i18n-iso-countries";

export const convertCountryToIso3Code = (country) => {
  const iso2 = countries.getAlpha2Code(country, "hr");
  return countries.alpha2ToAlpha3(iso2);
};
