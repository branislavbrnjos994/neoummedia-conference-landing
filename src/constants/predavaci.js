import AlexPercan from "../assets/avatars/AlexPercan.png";
import SinisaUbovic from "../assets/avatars/SinisaUbovic.png";
import SrdjanBogicevic from "../assets/avatars/SrdjanBogicevic.png";
import IvanaBrankovic from "../assets/avatars/IvanaBrankovic.png";
import SnezanaDakic from "../assets/avatars/SnezanaDakic.png";
import JadrankaPetrovic from "../assets/avatars/JadrankaPetrovic.png";
import MarkoMaodus from "../assets/avatars/MarkoMaodus.png";
import NaidaKundurovic from "../assets/avatars/NaidaKundurovic.png";
import StefanMarinkovic from "../assets/avatars/StefanMarinkovic.png";
import JelenaTomic from "../assets/avatars/JelenaTomic.png";
import GalaIsidoraRandjic from "../assets/avatars/GalaIsidoraRandjic.png";
import MarinaIlic from "../assets/avatars/MarinaIlic.png";
import LeaHorvat from "../assets/avatars/LeaHorvat.png";
import ZoranTubic from "../assets/avatars/ZoranTubic.png";
import MilicaMarkovic from "../assets/avatars/MilicaMarkovic.png";
import BrankaSelenic from "../assets/avatars/BrankaSelenic.png";
import AlexPercan2X from "../assets/avatars/AlexPercan@2x.png";
import SinisaUbovic2X from "../assets/avatars/SinisaUbovic@2x.png";
import SrdjanBogicevic2X from "../assets/avatars/SrdjanBogicevic@2x.png";
import IvanaBrankovic2X from "../assets/avatars/IvanaBrankovic@2x.png";
import SnezanaDakic2X from "../assets/avatars/SnezanaDakic@2x.png";
import JadrankaPetrovic2X from "../assets/avatars/JadrankaPetrovic@2x.png";
import MarkoMaodus2X from "../assets/avatars/MarkoMaodus@2x.png";
import NaidaKundurovic2X from "../assets/avatars/NaidaKundurovic@2x.png";
import StefanMarinkovic2X from "../assets/avatars/StefanMarinkovic@2x.png";
import JelenaTomic2X from "../assets/avatars/JelenaTomic@2x.png";
import GalaIsidoraRandjic2X from "../assets/avatars/GalaIsidoraRandjic@2x.png";
import MarinaIlic2X from "../assets/avatars/MarinaIlic@2x.png";
import LeaHorvat2X from "../assets/avatars/LeaHorvat@2x.png";
import ZoranTubic2X from "../assets/avatars/ZoranTubic@2x.png";
import MilicaMarkovic2X from "../assets/avatars/MilicaMarkovic@2x.png";
import BrankaSelenic2X from "../assets/avatars/BrankaSelenic@2x.png";

export const predavaci = [
  {
    image: SinisaUbovic,
    imageBig: SinisaUbovic2X,
    to: "/educator/0",
    name: "Siniša Ubović",
    profession: "Glumac, pisac & profesionalni kouč",
    time: "17.05 - 18.00",
    description:
      "Siniša Ubović je glumac, pisac i međunarodno priznat trener ličnog razvoja. Vodeća je ličnost na polju motivacije, savremenog liderstva i primenjene duhovnosti. Autor je sedam knjiga od kojih su gotovo sve automatski postali bestseleri. Više od 20 000 ljudi je prisustvovalo njegovim predavanjima, radionicama i seminarima u poslednjih nekoliko godina. Njegova učenja pomažu ljudima da otkriju i primene puni potencijal sopstvenih snaga na putu ličnog napretka u oblsti karijere, ljubavi, finansija, međuljudskih odnosa itd.",
  },
  {
    image: SrdjanBogicevic,
    imageBig: SrdjanBogicevic2X,
    to: "/educator/1",
    name: "Srđan Bogićević",
    profession: "Neuroistraživač, pisac & autor",
    time: "10.10 - 10.40",
    description:
      "Srđan Bogićević je neuroistraživač, pisac i autor dve knjige: The Art of Imagination & Reprogramiranje Uma. Sa svojim znanjem iz oblasti neurobiologije je doneo nešto novo na prostoru Balkana, brzo osvajajaći ljude jednostavnim objašnjenjem kompleksnih naučnih činjenica na način da svaka osoba može razumeti o čemu priča. Njegova autentičnost i harizma su doveli do nastanka najveće platforme praktičnog znanja na Balkanu pod nazivom NeoUm, na kojoj je okupio najbolje edukatore iz oblasti psihologije, nauke i duhovnosti, kako bi podelili svoje znanje i vama pomogli da kreirate transformaciju koju želite. Srđan je bio gost na mnogim podkast i TV emisijama gde je podelio svoje znanje o promeni emocionalnih stanja i reprogramiranja uma kako biste naučili svoj um da radi za vas da bi ostvarli ono što želite.",
  },
  {
    image: NaidaKundurovic,
    imageBig: NaidaKundurovic2X,
    to: "/educator/2",
    name: "Naida Kundurović",
    profession: "Psihoterapeut, voditelj & producent",
    time: "16.25 - 17.00",
    description:
      "Naida Kundurović je po zanimanju psihoterapeut, novinar i TV voditelj, koja je u suštini jedna divna duša koja je posvetila svoj život traganju za mirnim srcem i aktivnom kreiranju života kakav želi živeti. Sadržaj koji kreira što na društvenim mrežama, što na njenom website-u i drugim platformama želi potaknuti i druge da pronađu svoju istinu. Naida kreira razne programe iz sfere duhovnosti, rasta i razvoja svesnog života. Njena učenja su jedinstvena na prostoru Balkana i kao takva je privukla ogromnu publiku koja veoma ceni ono što ima da kaže.",
  },
  {
    image: SnezanaDakic,
    imageBig: SnezanaDakic2X,
    to: "/educator/3",
    name: "Snežana Dakić",
    profession: "NLP master, TV autor & Voditelj",
    time: "10.45 - 11.15",
    description:
      'Snežana Dakić je poznati TV autor & Voditelj, kao i sertifikovani trener ličnog razvoja čija karijera traje preko 30 godina. Njena autorska TV emisija "U trendu" / "Život u trendu" sa uspehom se prikazivala 22 godine na tri televizije i stekla kultni status i postala prepoznatljiv brend za sve generacije gledalaca. Snežana je takođe osvajala prestižne nagrada za ličnost godine magazina "Hello", nagradu publike i medija za doprinos i stvaralaštvo, kao i nagradu za "naj žena" 21 veka.  Trenutno vodi itekako uspešnu emisiju "Male stvari", a ujedno i edukuje ogromnu publiku na društvenim mrežama o tome kako da žive kvalitetnijim životom.',
  },
  {
    image: JadrankaPetrovic,
    imageBig: JadrankaPetrovic2X,
    to: "/educator/4",
    name: "Jadranka Petrović",
    profession: "Edukator & motivator",
    time: "13.35 - 14.05",
    description:
      "Jadranka Petrović je life kouč, motivator i pre svega svesna mlada žena koja uči drugi kako da žive svesno i srećno. Njena energija, iskrenost i autentičnost je nešto što je privuklo preko 7000 ljudi da posete njene edukacije i seminare na kojima deli svoja znanja iz ličnog iskustva i duhovnosti. Jadranka je posvećena ljudima i želi da im pomogne da se izdignu sa dna do samog vrha i kako da promene životnu percepciju, doživljaj sebe i svoje svrhe. Jedan je od edukatora prestižnog Ada Divine Festivala, kao i dobitnik mnogih nagrada iz preduzetništva, dok, kako sama priznaje, najveća nagrada su joj rezultati hiljada ljudi sa kojima je do sada sarađivala.",
  },
  {
    image: MarkoMaodus,
    imageBig: MarkoMaodus2X,
    to: "/educator/5",
    name: "Marko Maoduš",
    profession: "Profesor, Pisac & Edukator",
    time: "14.10 - 14.40",
    description:
      "Marko Maoduš je profesor, pisac i autor nekoliko knjiga koje su osvojila srca publike jednostavnošću shvatanja promene života. Marko je strastveni duhovni tragalac i predavač koji aktivnost vodi seminare i radionice koji se tiču različitih aspekata našeg ličnog i duhovnog razvoja. Njegov životni poziv je usmeren ka ličnom razvoju i pomaganju ljudima da se ponovo povežu sa samim sobom i da slede svoje srce. U okviru svojih radionica i programa, Marko sve učesnike vodi kroz različite procese i tehnike koje je najpre on sam prošao i čije rezultate je doživeo u sopstvenom životu.",
  },
  {
    image: IvanaBrankovic,
    imageBig: IvanaBrankovic2X,
    to: "/educator/6",
    name: "Ivana Branković",
    profession: "Magistar muzičke pedagogije",
    time: "14.45 - 15.45",
    description:
      'Ivana Branković je vokalni trener, magistar muzike, profesor pevanja, vokalni solista i kreator programa "Pronađi svoj glas" kroz tehnike javnog nastupa. Obučava ljude kako da pravilno koriste svoj glas i razviju samopouzdanje. Takođe vas uči kako da pravilno koristite svoj glas od snimanja audio ili video kontenta za društvene mreže, do uživo nastupa i kako da koristite asertivnu komunikaciju u privatnom i poslovnom životu.',
  },
  {
    image: StefanMarinkovic,
    imageBig: StefanMarinkovic2X,
    to: "/educator/7",
    name: "Stefan Marinković",
    profession: "Preduzetnik & pisac",
    time: "11.20 - 12.20",
    description:
      'Stefan Marinković je preduzetnik, mentor i pisac koji ima ogromnu veru kako u Boga, tako i u sebe i njegova misija je da pomogne ljudima da pronađu svoju svrhu i izdignu se iznad prosečnog života. Njegova vera i snaga koju poseduje se preslikava na ogroman broj ljudi koji prati i poštuje njegov rad na društvenim mrežama. Autor je knjige "Džentlmen za 1 dan" koja je bila toliko tražena da je do sada odštampano tri tiraža i pored toga se traži kopija više.',
  },
  {
    image: JelenaTomic,
    imageBig: JelenaTomic2X,
    to: "/educator/8",
    name: "Jelena Tomić",
    profession: "Preduzetnica & Terapeut",
    time: "11.20 - 12.20",
    description:
      "Jelena Tomić je Personal Life Coach Therapist i regresoterapeut koja pomaže ljudima da se povežu sa sobom tako da mogu živeti srećan i ispunjen život. Takodje je i preduzetnica i ima veliko životno iskustvo u sferi ličnog razvoja. Prošla je kroz puno ličnih transformacija i sada živi život sa svrhom. Životna misija joj je da dodaje vrednost drugim ljudima kako bi živeli u skladu sa sobom.",
  },
  {
    image: GalaIsidoraRandjic,
    imageBig: GalaIsidoraRandjic2X,
    to: "/educator/9",
    name: "Gala Isidora Ranđić",
    profession: "Psiholog & Psihoterapeut",
    time: "11.20 - 12.20",
    description:
      "Gala Isidora Ranđić je diplomirani psiholog, master menadžer ljudskih resursa sa desetogodišnjim iskustvom rada sa ljudima, sertifikovani PCM trener i integrativni psihoterapeut. Osnivač je i vlasnik agencije za ljudske resurse HUMART. Njena misija je da pomogne ljudima da žive svoju viziju života, da otkriju svoj potencijal, promene štetne obrasce i steknu veštine koje su im neophodne za ostvarenje potencijala i dostizanje najskladnije verzije sebe.",
  },
  {
    image: AlexPercan,
    imageBig: AlexPercan2X,
    to: "/educator/10",
    name: "Alex Percan",
    profession: "Kineziolog & Fitness trener",
    time: "11.20 - 12.20",
    description:
      "Alex Percan je kineziolog, fitness trener i bivši košarkaš koji je po povratku u Hrvatsku odlučio otvoriti jednu od najboljih teretana u Hrvatskoj (Pula) sa svojom suprugom pod nazivom AP Lab. Alex je zaljubljenik u kvalitetan i ispunjen život. Mentor je brojnim trenerima i nesebičan edukator koji je jedno pravo osveženje na našim prostorima i čija misija je da podstakne što veći broj ljudi da žive zdravim i aktivnim načinom života.",
  },
  {
    image: LeaHorvat,
    imageBig: LeaHorvat2X,
    to: "/educator/11",
    name: "Lea Horvat",
    profession: "Preduzetnica & Holistički Terapeut",
    time: "11.20 - 12.20",
    description:
      "Lea Horvat je preduzetnica, holistički terapeut, NLP master i reiki učitelj. Sve što radi je pre svega njen poziv, ne samo posao. Dušom je odabrala biti žena koja je sebi svoja. Sve što danas radi ima svrhu da uči još više o sebi, a time pomogne i drugim ljudima kako bi im olakšala put ličnog rasta i razvoja. Iskreno veruje i zna da smo kreatori svog života i da putem kojim hodamo srećemo ko-kreatore koji nam pomažu da ostvarimo sve što želimo.",
  },
  {
    image: MarinaIlic,
    imageBig: MarinaIlic2X,
    to: "/educator/12",
    name: "Marina Ilić",
    profession: "Fitness trener & savetnik za ishranu",
    time: "14.45 - 15.45",
    description:
      "Marina Ilić je preduzetnica, fitnes trener, specijalista za korektivne vežbe za trudnice i porodilje i savetnik za ishranu i suplementaciju. Oblikuje, jača i motiviše ženska tela, duše i srca. Veruje da svako od nas ima moć da bude baš ono što poželi kroz učenje, lični razvoj i akciju. Voli da drugima pokazuje da mogu uraditi ono što žele, čak i kada misle da je to nemoguće.",
  },
  {
    image: ZoranTubic,
    imageBig: ZoranTubic2X,
    to: "/educator/13",
    name: "Zoran Tubić",
    profession: "Pisac, autor i psihološki savetnik u edukaciji",
    time: "14.45 - 15.45",
    description:
      "Zoran Tubić je pisac, autor i psihološki savetnik u edukaciji koji je dugo razmišljao šta je to što svakoj osobi koja se suočava sa anksioznošću treba. Kroz svoje knjige, priručnike i edukacije pomaže ljudima da se oslobode anksioznosti jednom za sva vremena. Njegovo iskustvo je dragoceno i mnogo mu pomaže da bolje razume ljude, ali od velike pomoći su mu i teorijska znanja, pre svega iz transakcionie analize, psihoterapijskog pravca i iz drugih izvora iz kojih neprestano uči.",
  },
  {
    image: MilicaMarkovic,
    imageBig: MilicaMarkovic2X,
    to: "/educator/14",
    name: "Milica Marković",
    profession: "Ekspert za lične granice",
    time: "14.45 - 15.45",
    description:
      'Milica Marković je preduzetnica, klinički psiholog i psihoterapeut. Na temelju višegodišnjeg psihoterapijskog rada, iskustva rada u više startup timova kako u zemlji, tako i u inostranstvu, kao i iskustvu izgradnje svog biznisa, kreirala je program "Granice za preduzetnice" gde uči žene kako da postupaju u skladu sa sobom, imaju mir u svom danu, stabilnost u svom biznisu i bliske odnose sa porodicom.',
  },
  {
    image: BrankaSelenic,
    imageBig: BrankaSelenic2X,
    to: "/educator/15",
    name: "Branka Selenić",
    profession: "Kouč za pregoravanje i brigu o sebi",
    time: "14.45 - 15.45",
    description:
      "Branka Selenić je preduzetnica i profesionalni kouč koji vam pomaže da naučite da brinete o sebi na pravi način. Brankina strast je u pomaganju drugima jer kroz taj proces i sama raste i osnažuje sebe, a time i druge. Autor je nekoliko programa u kojima uči druge kako da brigom o sebi dođu do života u kojima su srećni, odmorni i zdravi, kao i kako da imaju više vremena za sebe i svoje najbliže.",
  },
];
