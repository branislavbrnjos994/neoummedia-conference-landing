import Paragraph from "../components/Paragraph";
import { ReactComponent as CiljeviIlustration } from "../assets/ilustration/ciljevi-ilustration.svg";
import { ReactComponent as StrategijeIlustration } from "../assets/ilustration/strategije-ilustration.svg";
import { ReactComponent as UverenjaIlustration } from "../assets/ilustration/uverenja-ilustration.svg";
import { ReactComponent as IzazoviIlustration } from "../assets/ilustration/izazovi-ilustration.svg";
import { ReactComponent as AkcijaIlustration } from "../assets/ilustration/akcija-ilustration.svg";

export const topics = [
  {
    name: "Ciljevi",
    title: (
      <>
        Da li ste definisani <span>vizijom svoje budućnosti?</span>
      </>
    ),
    description: (
      <>
        <Paragraph variant={"p2"} fontWeight={300}>
          Ukoliko se ne budite definisani vizijom svoje budućnosti, svakoga dana
          živite život kroz prizmu svoje prošlosti. Što znači da svakoga dana
          imate iste misli, koje konstruišu iste emocije, koje diktiraju ista
          ponašanja i <strong>sve to proizvodi iste rezultate</strong>. Kako
          možete očekivati da se vaš život promeni ukoliko nemate neki cilj?
        </Paragraph>
        <Paragraph variant="p2" fontWeight={300}>
          Upravo ćete na ovoj konferenciji dobiti ideju odakle da krenete ka
          boljem životu. Dobićete jasnu strukturu kako da definišete svoje
          ciljeve, <strong>kao i načine kako da ih ostvarite.</strong>
        </Paragraph>
      </>
    ),
    ilustration: <CiljeviIlustration />,
  },
  {
    name: "Plan & Strategije",
    title: (
      <>
        Svaki cilj zahteva <span>plan i strategije</span> za izvršenje tog
        plana.
      </>
    ),
    description: (
      <>
        <Paragraph variant={"p2"} fontWeight={300}>
          Nijedna uspešna osoba nije stigla do uspeha bez nekog plana. Tako ćete
          i vi dobiti jasnoću o tome <strong>kakav vam je plan potreban</strong>{" "}
          kako biste ostvarili svoje ciljeve.
        </Paragraph>
        <Paragraph variant="p2" fontWeight={300}>
          A za svaki plan je potrebna strategija za izvršenje tog plana i{" "}
          <strong>na konferenciji ćete dobiti razne ideje</strong> o tome kako
          možete da definišete svoje planove i time ostvarite svoje ciljeve.
        </Paragraph>
      </>
    ),
    ilustration: <StrategijeIlustration />,
  },
  {
    name: "Uverenja",
    title: (
      <>
        Na putu ka ciljevima, <span>vaš sistem uverenja će biti izazvan</span>.
      </>
    ),
    description: (
      <>
        <Paragraph variant={"p2"} fontWeight={300}>
          Onoga trenutka kada preduzmete praktične korake ka svojim ciljevima -{" "}
          <strong>budite spremni</strong>! Zašto? Sva vaša negativna i
          limitirana uverenja će isplivati na površinu sa kojima ćete morati da
          se suočite.
        </Paragraph>
        <Paragraph variant="p2" fontWeight={300}>
          Na konferenciji ćete dobiti praktične alate i tehnike kako da
          promenite svoja negativna i limitirana uverenja i naučite svoj um da
          radi za vas <strong>kako biste ostvarili svoje ciljeve</strong>.
        </Paragraph>
      </>
    ),
    ilustration: <UverenjaIlustration />,
  },
  {
    name: "Izazovi",
    title: (
      <>
        Izazovi i prepreke su deo svakog <span>herojskog puta</span>.
      </>
    ),
    description: (
      <>
        <Paragraph variant={"p2"} fontWeight={300}>
          Svaki edukator koji će predavati na konferenciji je prošao kroz trnje,
          oluje i razne izazove koji su ih mogli naterati da odustanu, ali ipak
          to nisu učinili! Zašto?{" "}
          <strong>
            Imali su velike razloge da nastave dalje, čak i kada je sve bilo
            protiv njih.
          </strong>
        </Paragraph>
        <Paragraph variant="p2" fontWeight={300}>
          <strong>Svako od njih će podeliti svoje znanje i iskustvo</strong> o
          tome kako da se nosite sa raznim izazovima počev od straha, negativnog
          okruženja, limitiranih uverenja i mnogih drugih koji su im se našli na
          putu ka uspehu.
        </Paragraph>
      </>
    ),
    ilustration: <IzazoviIlustration />,
  },
  {
    name: "Akcija & Alati",
    title: (
      <>
        Bez <span>akcije</span> nema rezultata.
      </>
    ),
    description: (
      <>
        <Paragraph variant={"p2"} fontWeight={300}>
          Kada postavimo cilj,{" "}
          <strong>svi smo uzbuđeni i motivisani na početku</strong>. A šta ćemo
          kada to početno uzbuđenje i motivacija nestanu? Kada nas limitirana i
          negativna uverenja krenu sabotirati? Kada nas okruženje sputava? Kada
          negativne misli upravljaju našim ponašanjima?
        </Paragraph>
        <Paragraph variant={"p2"} fontWeight={"bold"}>
          Kako nastaviti dalje? Koje akcije preduzeti?
        </Paragraph>
        <Paragraph variant="p2" fontWeight={300}>
          Upravo ćete na konferenciji naučiti kako i kada koje akcije da
          preduzmete uz pomoć praktičnih protokola, alata i strategija koje će
          naši edukatori podeliti sa vama. Njihov život je njihovo svedočanstvo
          koje pokazuje šta je sve moguće{" "}
          <strong>
            ukoliko nastavite da se pojavljujete i preduzimate praktične
          </strong>{" "}
          korake (a koje ćete vi naučiti) svakoga dana.
        </Paragraph>
      </>
    ),
    ilustration: <AkcijaIlustration />,
  },
];
