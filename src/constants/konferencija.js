import ConferenceOne from "../assets/images/conference-1.png";
import ConferenceTwo from "../assets/images/conference-2.png";
import ConferenceThree from "../assets/images/conference-3.png";

export const konferencija = [
  {
    image: ConferenceOne,
    alt: "Conference One",
  },
  {
    image: ConferenceTwo,
    alt: "Conference Two",
  },
  {
    image: ConferenceThree,
    alt: "Conference Three",
  },
];
