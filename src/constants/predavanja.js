import SrdjanBogicevic from "../assets/program/SrdjanBogicevic.png";
import IvanaBrankovic from "../assets/program/IvanaBrankovic.png";
import SnezanaDakic from "../assets/program/SnezanaDakic.png";
import JadrankaPetrovic from "../assets/program/JadrankaPetrovic.png";
import MarkoMaodus from "../assets/program/MarkoMaodus.png";
import SinisaUbovic from "../assets/program/SinisaUbovic.png";
import NaidaKundurovic from "../assets/program/NaidaKundurovic.png";

export const predavanja = [
  {
    time: "09.15 - 09.55",
    title: "Registracija učesnika",
    subtitle: "",
  },
  {
    time: "10.00 - 10.10",
    title: "Svečano otvaranje konferencije",
    subtitle: "Ivana Branković",
    image: IvanaBrankovic,
  },
  {
    time: "10.10 - 10.40",
    title: "Srđan Bogićević",
    subtitle: "Neuroistraživač, pisac & autor",
    image: SrdjanBogicevic,
  },
  {
    time: "10.45 - 11.15",
    title: "Snežana Dakić",
    subtitle: "NLP master, TV autor & Voditelj",
    image: SnezanaDakic,
  },
  {
    time: "11.20 - 12.20",
    title: "Panel edukatora",
    subtitle:
      "Stefan Marinković - Jelena Tomić - Gala Isidora Ranđić - Alex Percan - Lea Horvat",
  },
  {
    time: "12.25 - 13.25",
    title: "Pauza",
    subtitle: "",
  },
  {
    time: "13.35 - 14.05",
    title: "Jadranka Petrović",
    subtitle: "Edukator & motivator",
    image: JadrankaPetrovic,
  },
  {
    time: "14.10 - 14.40",
    title: "Marko Maoduš",
    subtitle: "Profesor, Pisac & Edukator",
    image: MarkoMaodus,
  },
  {
    time: "14.45 - 15.45",
    title: "Panel edukatora",
    subtitle:
      "Zoran Tubić - Milica Marković - Marina Ilić - Branka Selenić - Ivana Branković",
  },
  {
    time: "15.45 - 16.00",
    title: "Prezentacija platforme",
    subtitle: "Srđan Bogićević",
    image: SrdjanBogicevic,
  },
  {
    time: "16.05 - 16.20",
    title: "Kratka pauza",
    subtitle: "",
  },
  {
    time: "16.25 - 17.00",
    title: "Naida Kundurović",
    subtitle: "Psihoterapeut, voditelj & producent",
    image: NaidaKundurovic,
  },
  {
    time: "17.05 - 17.40",
    title: "Siniša Ubović",
    subtitle: "Glumac, pisac & profesionalni kouč",
    image: SinisaUbovic,
  },
  {
    time: "17.45 - 18.00",
    title: "Zatvaranje konferencije",
  },
];
