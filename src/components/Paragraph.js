import React from "react";
import PropTypes from "prop-types";

const Paragraph = ({
  variant,
  children,
  fontWeight = "normal",
  fontFamily = "Montserrat",
  ...props
}) => {
  return (
    <p
      className={`paragraph paragraph--${variant}`}
      style={{ fontWeight: fontWeight, fontFamily: fontFamily }}
      {...props}
    >
      {children}
    </p>
  );
};

Paragraph.propTypes = {
  variant: PropTypes.oneOf(["title", "subtitle", "p1", "p2", "p3"]),
  fontWeight: PropTypes.oneOf([
    300,
    400,
    500,
    600,
    700,
    800,
    900,
    "bold",
    "normal",
  ]),
  children: PropTypes.node,
};

export default Paragraph;
