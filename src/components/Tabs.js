import React from "react";

const Tabs = ({ tabs, currentValue, variant }) => {
  return (
    <div className={`tabs tabs--${variant}`}>
      {tabs.map((tab) => (
        <button
          key={tab.value}
          onClick={tab?.onClick}
          className={` ${currentValue === tab.value && "is--active"}`}
        >
          {tab.icon && tab.icon}
          <span>{tab.label}</span>
        </button>
      ))}
    </div>
  );
};

export default Tabs;
