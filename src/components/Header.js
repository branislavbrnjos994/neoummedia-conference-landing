import React, { useEffect, useState } from "react";
import Paragraph from "./Paragraph";
import Button from "./Button";
import { useMediaQuery } from "react-responsive";
import { useLocation, useNavigate } from "react-router";
import { ReactComponent as MenuIcon } from "../assets/svg/menu.svg";
import Sidebar from "./Sidebar";

const Header = () => {
  const [isSidebarOpen, setIsSidebarOpen] = useState(false);

  const isLaptop = useMediaQuery({ query: "(max-width: 1040px)" });
  const isMobile = useMediaQuery({ query: "(max-width: 600px)" });
  const { pathname } = useLocation();
  const navigate = useNavigate();

  const handleClickScroll = (id) => {
    if (pathname.includes("/register") || pathname.includes("/educator")) {
      navigate("");
    }

    setTimeout(() => {
      const element = document.getElementById(id);
      if (element) {
        element.scrollIntoView({ behavior: "smooth" });
      }
    }, 0);
  };

  const handleClickScrollSidebar = (id) => {
    setIsSidebarOpen(false);
    if (pathname.includes("/register") || pathname.includes("/educator")) {
      navigate("");
    }

    setTimeout(() => {
      const element = document.getElementById(id);
      if (element) {
        element.scrollIntoView({ behavior: "smooth" });
      }
    }, 0);
  };

  useEffect(() => {
    if (pathname.includes("/register") || pathname.includes("/educator")) {
      window.scrollTo(0, 0);
    }
  }, [pathname]);

  return (
    <>
      <header
        className={`header ${
          pathname.includes("/register") || pathname.includes("/educator")
            ? "header--primary"
            : ""
        }`}
      >
        <h1 onClick={() => navigate("/")} className="header__logo">
          <span>NeoUm</span>
        </h1>
        {!isLaptop && (
          <nav className="header__nav">
            <button onClick={() => handleClickScroll("edukatori")}>
              Predavači
            </button>
            <button onClick={() => handleClickScroll("program")}>
              Program
            </button>
            <button onClick={() => handleClickScroll("ulaznice")}>
              Ulaznice
            </button>
            <button onClick={() => handleClickScroll("kontakt")}>
              Kontakt
            </button>
          </nav>
        )}
        <div className="header__cta">
          <div className="header__conference">
            <Paragraph variant={"p3"} fontWeight={600}>
              11.11.2023.
            </Paragraph>
            <Paragraph variant={"p3"}>Beograd</Paragraph>
          </div>
          {!isMobile && (
            <Button
              onClick={() => navigate("/register")}
              variant={"primary"}
              size="small"
            >
              Prijavi se
            </Button>
          )}
          {isLaptop && (
            <button
              onClick={() => setIsSidebarOpen(true)}
              type="button"
              className="header__menu-button"
            >
              <MenuIcon />
            </button>
          )}
        </div>
      </header>
      {isSidebarOpen && (
        <Sidebar
          onClose={() => setIsSidebarOpen(false)}
          onNavigate={handleClickScrollSidebar}
        />
      )}
      {isMobile &&
        !(pathname.includes("/register") || pathname.includes("/purchase")) && (
          <div className="header__footer">
            <Button
              onClick={() => navigate("register")}
              variant={"primary"}
              size="small"
            >
              Prijavi se
            </Button>
            <Button
              onClick={() => handleClickScroll("ulaznice")}
              variant={"primary-outlined"}
              size="small"
            >
              Ulaznice
            </Button>
          </div>
        )}
    </>
  );
};

export default Header;
