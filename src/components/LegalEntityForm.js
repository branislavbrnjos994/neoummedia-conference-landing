import React from "react";
import PropTypes from "prop-types";
import { Formik, Form } from "formik";
import Input from "./Input";
import Paragraph from "./Paragraph";
import { useMediaQuery } from "react-responsive";
import Benefits from "./Benefits";
import { ReactComponent as ProfileIlustration } from "../assets/ilustration/profile-ilustration.svg";
import Button from "./Button";
import { useCountries } from "../hooks/useCountries";
import Autocomplete from "./Autocomplete";

const LegalEntityForm = ({
  schema,
  handleSubmit,
  extraTickets,
  extraTicketsCount,
  isLoading,
  policies,
}) => {
  const isMediumScreen = useMediaQuery({ query: "(max-width: 1400px)" });
  const { countries } = useCountries();
  const isSmallScreen = useMediaQuery({ query: "(max-width: 1000px)" });

  return (
    <Formik
      initialValues={{
        bearerFullName: "",
        bearerEmail: "",
        taxId: "",
        country: "",
        city: "",
        address: "",
        question: "",
        name1: "",
        name2: "",
        name3: "",
        name4: "",
        email1: "",
        email2: "",
        email3: "",
        email4: "",
        liveStreamPolicy: false,
        refundPolicy: false,
      }}
      validationSchema={schema}
      onSubmit={handleSubmit}
    >
      <Form className="register__form">
        <div className="register__field-group">
          <Input variant="dark" name="firm" label="Naziv firme *" />
          <Input variant="dark" name="taxId" label="PIB/OIB *" />
        </div>
        <div className="register__field-group">
          <Autocomplete
            name="country"
            label="Država *"
            placeholder="Unesite Državu"
            type="text"
            variant={"dark"}
            items={Object.values(countries)}
          />
          <Input variant="dark" name="city" label="Grad *" />
        </div>
        <Input variant="dark" name="address" label="Adresa i broj" />
        <div className="register__field-group">
          <Input variant="dark" name="bearerFullName" label="Ime i prezime *" />
          <Input variant="dark" name="bearerEmail" label="Email *" />
        </div>
        <Input
          variant="dark"
          name="question"
          label="Pitanje na temu “Neograničeni um”"
          message={
            "Postavite jedno pitanje na koje ćemo odgovoriti u toku panel predavanja."
          }
        />
        {policies}
        {extraTickets}
        <div className="register__text">
          <Paragraph variant={"p2"}>
            Obratite pažnju da li ste uneli tačne podatke, ukoliko imate nekih
            pitanja ili nejasnoća možete nam se obratiti na email{" "}
            <a href="mailto:neoumkonferencija@gmail.com">
              neoumkonferencija@gmail.com
            </a>
          </Paragraph>
          <Paragraph variant={"p2"}>
            Vašu email adresu nećemo deliti ni sa kim. Kupovinom prihvatate
            slanje promotivnih emailova, mozete se odjaviti sa liste u bilo kom
            trenutku.
          </Paragraph>
        </div>
        {isMediumScreen && (
          <div className="register__tickets">
            <div className="register__includes">
              <div className="register__badge">
                <h2>Ulaznica obuhvata</h2>
              </div>
              <Benefits />
            </div>
            <div className="register__pricing">
              <div className="register__badge">
                Cena {extraTicketsCount === 0 ? "ulaznice" : "ulaznica"}{" "}
                <span>{(extraTicketsCount + 1) * 44}€</span>
              </div>
              <div className="register__image">
                <ProfileIlustration />
              </div>
            </div>
          </div>
        )}
        {!isSmallScreen && (
          <Button type="submit" variant="tertiary" isLoading={isLoading}>
            Nastavi plaćanje
          </Button>
        )}
        {isSmallScreen && (
          <div className="header__footer">
            <Button type="submit" variant="tertiary" isLoading={isLoading}>
              Nastavi plaćanje
            </Button>
          </div>
        )}
      </Form>
    </Formik>
  );
};

LegalEntityForm.propTypes = {
  schema: PropTypes.object,
  handleSubmit: PropTypes.func,
  extraTickets: PropTypes.node,
  extraTicketsCount: PropTypes.number,
};

export default LegalEntityForm;
