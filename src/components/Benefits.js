import React from "react";
import { benefiti } from "../constants/benefiti";
import { ReactComponent as ConfirmIcon } from "../assets/svg/confirm.svg";
import Paragraph from "./Paragraph";

const Benefits = () => {
  return (
    <ul className="benefits">
      {benefiti.map((item) => (
        <li key={item}>
          <span>
            <ConfirmIcon />
          </span>{" "}
          <Paragraph variant={"p2"} fontWeight={500}>
            {item}
          </Paragraph>
        </li>
      ))}
    </ul>
  );
};

export default Benefits;
