import React from "react";
import PropTypes from "prop-types";

const Tag = ({ children, onClick, isActive }) => {
  return (
    <button className={`tag ${isActive && "tag--active"}`} onClick={onClick}>
      {children}
    </button>
  );
};

Tag.propTypes = {
  onClick: PropTypes.func,
  children: PropTypes.element,
};

export default Tag;
