import React from "react";
import { ReactComponent as CancelIcon } from "../assets/svg/cancel.svg";
import Button from "./Button";
import Paragraph from "./Paragraph";
import { useNavigate } from "react-router";

const Sidebar = ({ onClose, onNavigate }) => {
  const navigate = useNavigate();

  return (
    <div className="sidebar">
      <div className="sidebar__header">
        <h1 className="sidebar__title">NeoUm</h1>
        <button onClick={onClose} className="sidebar__cancel">
          <CancelIcon />
        </button>
      </div>
      <div className="sidebar__content">
        <div className="sidebar__conference">
          <Paragraph variant={"title"} fontWeight={500}>
            11. Novembar
          </Paragraph>
          <Paragraph variant={"subtitle"} fontWeight={300}>
            Madlenianum, Beograd
          </Paragraph>
        </div>

        <h1 className="sidebar__logo">
          NeoUm<br></br>
          <span>Konferencija</span>
          <br></br>2023
        </h1>

        <nav className="sidebar__navigation">
          <button onClick={() => onNavigate("edukatori")}>Predavači</button>
          <button onClick={() => onNavigate("program")}>Program</button>
          <button onClick={() => onNavigate("ulaznice")}>Ulaznice</button>
          <button onClick={() => onNavigate("kontakt")}>Kontakt</button>
        </nav>
      </div>
      <div className="sidebar__cta">
        <Button variant={"primary"} onClick={() => navigate("register")}>
          Prijavi se
        </Button>
      </div>
    </div>
  );
};

export default Sidebar;
