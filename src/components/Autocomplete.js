/* eslint-disable react-hooks/rules-of-hooks */
import { memo, useEffect, useMemo, useRef, useState } from "react";
import { useField } from "formik";
import isEmpty from "lodash.isempty";

const Autocomplete = ({ label, id, name, type, items, variant, ...props }) => {
  const [field, meta] = useField(name);
  const [isFocused, setIsFocused] = useState(false);
  const [dropdownItems, setDropdownItems] = useState([]);
  const [isDropdownOpen, setIsDropdownOpen] = useState(false);

  const useClickOutside = (ref, callback) => {
    const handleClick = (e) => {
      if (ref.current && !ref.current.contains(e.target)) {
        callback();
      }
    };

    useEffect(() => {
      document.addEventListener("click", handleClick);
      return () => {
        document.removeEventListener("click", handleClick);
      };
    });
  };

  const autocompleteRef = useRef(null);

  const [isLabelFloating, setIsLabelFloating] = useState(
    !!field.value || isFocused
  );

  useEffect(() => {
    setIsLabelFloating(!!field.value || isFocused);
  }, [field.value, isFocused]);

  const autofillField = useMemo(
    () => (value) => {
      field.onChange({ target: { name, value } });
      setIsDropdownOpen(false);
    },
    [field, name]
  );

  useEffect(() => {
    if (field.value) {
      setDropdownItems(
        items.filter((item) =>
          item.toLowerCase().includes(field.value.toLowerCase())
        )
      );
      return;
    }

    setDropdownItems(items);
  }, [field.value, items]);

  useClickOutside(
    autocompleteRef,
    () => isDropdownOpen && setIsDropdownOpen(false)
  );

  return (
    <div
      className={`input ${isLabelFloating ? "input--floating" : ""} ${
        isFocused ? "input--focused" : ""
      } ${meta.error && meta.touched ? "input--error" : ""} ${
        variant && `input--${variant}`
      }`}
      ref={autocompleteRef}
    >
      <div className="input_inner">
        <label
          className={`input_label ${
            isLabelFloating ? "input_label--floating" : ""
          }`}
          htmlFor={name}
        >
          {label}
        </label>
        <div className="input_wrap">
          <input
            id={name}
            className="input_field"
            {...field}
            {...props}
            onFocus={() => {
              setIsDropdownOpen(true);
              setIsFocused(true);
            }}
            onBlur={(event) => {
              setIsFocused(false);
              field.onBlur(event);
            }}
            placeholder={isLabelFloating ? props?.placeholder : ""}
            type={"text"}
          />
        </div>

        {isDropdownOpen && (
          <div className="input_suggestions-dropdown">
            {!isEmpty(dropdownItems) && (
              <ul>
                {dropdownItems.map((item) => (
                  <li
                    key={item}
                    className={`${
                      item === field.value
                        ? "input_suggestions-item--selected"
                        : ""
                    }`}
                    onClick={(event) => {
                      event?.stopPropagation();
                      autofillField(item);
                    }}
                  >
                    <span>
                      <span />
                    </span>
                    <span>{item}</span>
                  </li>
                ))}
              </ul>
            )}
            {isEmpty(dropdownItems) && (
              <ul>
                <li className="input_suggestions-item--selected">
                  <span>
                    <span />
                  </span>
                  <span>Ne postoji</span>
                </li>
              </ul>
            )}
          </div>
        )}
      </div>
      <span className="input_error">
        {(meta.error && meta.touched && meta.error) || "error"}
      </span>
    </div>
  );
};

export default memo(Autocomplete);
