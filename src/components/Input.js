/* eslint-disable react-hooks/rules-of-hooks */
import { useEffect, useRef, useState } from "react";
import { useField } from "formik";

const Input = ({ label, id, name, type, message, variant, ...props }) => {
  const [field, meta] = useField(name);
  const [isFocused, setIsFocused] = useState(false);
  const inputRef = useRef(null);

  const [isLabelFloating, setIsLabelFloating] = useState(
    !!field.value || isFocused
  );

  useEffect(() => {
    setIsLabelFloating(!!field.value || isFocused);
  }, [field.value, isFocused]);

  const formatLabel = (label) => {
    if (label.includes("*")) {
      return (
        <>
          {`${label.replace("*", "")}`} <span>*</span>
        </>
      );
    }

    return label;
  };

  return (
    <div
      className={`input ${isLabelFloating ? "input--floating" : ""} ${
        isFocused ? "input--focused" : ""
      } ${meta.error && meta.touched ? "input--error" : ""} ${
        variant && `input--${variant}`
      }`}
    >
      <div className="input_inner">
        <label
          className={`input_label ${
            isLabelFloating ? "input_label--floating" : ""
          }`}
          htmlFor={name}
        >
          {formatLabel(label)}
        </label>
        <div className="input_wrap">
          <input
            id={name}
            className="input_field"
            ref={inputRef}
            {...field}
            {...props}
            onFocus={() => setIsFocused(true)}
            onBlur={(event) => {
              setIsFocused(false);
              field.onBlur(event);
            }}
            placeholder={isLabelFloating ? props?.placeholder : ""}
            type={type}
          />
        </div>
      </div>
      <span className={`${message ? "input_message" : "input_error"}`}>
        {(meta.error && meta.touched && meta.error) || message || "error"}
      </span>
    </div>
  );
};

export default Input;
