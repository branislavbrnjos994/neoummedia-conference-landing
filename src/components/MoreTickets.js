import React from "react";
import Button from "./Button";
import Input from "./Input";
import { ReactComponent as TrashIcon } from "../assets/svg/delete.svg";

const MoreTickets = ({ extraTicketsCount, setExtraTicketsCount }) => {
  const addExtraTicket = () => {
    if (extraTicketsCount < 4) {
      setExtraTicketsCount(extraTicketsCount + 1);
    }
  };

  const removeExtraTicket = () => {
    setExtraTicketsCount(extraTicketsCount - 1);
  };

  const renderExtraTicketFields = () => {
    return Array(extraTicketsCount)
      .fill()
      .map((_, index) => (
        <div className="register__extra-field-group" key={index}>
          <div className="register__field-group">
            <Input
              variant="dark"
              name={`name${index + 1}`}
              label={`Ime i prezime *`}
            />
            <Input
              variant="dark"
              name={`email${index + 1}`}
              label={`Email *`}
            />
          </div>
          <Button
            type="button"
            onClick={() => removeExtraTicket(index)}
            variant="danger"
          >
            <TrashIcon />
          </Button>
        </div>
      ));
  };

  return (
    <div className="register__extra-tickets">
      {extraTicketsCount > 0 && (
        <h3> Dodatne ulaznice - {extraTicketsCount}</h3>
      )}
      <div>{renderExtraTicketFields()}</div>
      <Button
        onClick={addExtraTicket}
        disabled={extraTicketsCount >= 4}
        variant="outlined"
        type="button"
      >
        Prijavi još osoba
      </Button>
    </div>
  );
};

export default MoreTickets;
