import React from "react";
import PropTypes from "prop-types";
import { ReactComponent as LoaderIcon } from "../assets/svg/loader.svg";

const Button = ({
  variant,
  children = "primary",
  size = "big",
  isLoading,
  isDisabled,
  ...props
}) => {
  return (
    <button
      {...props}
      disabled={isDisabled || isLoading}
      className={`button button--${variant} button--${size}`}
    >
      {children}
      {isLoading && (
        <span className="button_loader">{isLoading && <LoaderIcon />}</span>
      )}
    </button>
  );
};

Button.propTypes = {
  variant: PropTypes.oneOf([
    "primary",
    "secondary",
    "tertiary",
    "outlined",
    "primary-outlined",
    "danger",
  ]),
  children: PropTypes.node,
  size: PropTypes.oneOf(["small", "big"]),
};

export default Button;
