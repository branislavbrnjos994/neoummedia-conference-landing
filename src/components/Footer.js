import React from "react";
import Paragraph from "./Paragraph";
import { ReactComponent as EmailIlustration } from "../assets/svg/email-ilustration.svg";
import { ReactComponent as SendIcon } from "../assets/svg/send.svg";

import { Form, Formik } from "formik";
import Input from "./Input";
import Button from "./Button";
import { useMediaQuery } from "react-responsive";
import { useLocation } from "react-router";
import * as yup from "yup";
import { POST } from "../services/requests";
import { useMutation } from "@tanstack/react-query";
import { toast } from "react-toastify";

const Footer = (props) => {
  const isBigScreen = useMediaQuery({ query: "(max-width: 1350px)" });
  const { pathname } = useLocation();

  const contactSchema = yup.object({
    fullName: yup.string().required("Ime i prezime je obavezno polje."),
    email: yup
      .string()
      .email("Email mora biti ispravnog formata.")
      .required("Email je obavezno polje."),
    message: yup.string().required("Poruka je obavezno polje."),
  });

  const { mutate, isLoading } = useMutation((payload) =>
    POST("/conference/contact", payload)
  );

  const handleSubmit = (values) => {
    mutate(values, {
      onSuccess: () => {
        toast("Poruka je uspešno poslata!");
      },
      onError: () => {
        toast("Poruka nije uspešno poslata!");
      },
    });
  };

  return (
    <footer className="footer" id="kontakt">
      {!(
        pathname.includes("/register") ||
        pathname.includes("/educator") ||
        pathname.includes("/purchase")
      ) && (
        <section className="footer__contact">
          {!isBigScreen && (
            <div className="footer__content">
              <h1>Kontakt</h1>
              <EmailIlustration />
              <Paragraph variant={"p2"}>
                Ukoliko imate nekih pitanja ili nejasnoća možete nam se obratiti
                putem kontakt forme ili na email{" "}
                <a href="mailto:neoumkonferencija@gmail.com">
                  neoumkonferencija@gmail.com
                </a>
              </Paragraph>
            </div>
          )}
          {isBigScreen && (
            <div className="footer__content">
              <div>
                <h1>Kontakt</h1>
                <Paragraph variant={"p2"}>
                  Ukoliko imate nekih pitanja ili nejasnoća možete nam se
                  obratiti putem kontakt forme ili na email{" "}
                  <a href="mailto:neoumkonferencija@gmail.com">
                    neoumkonferencija@gmail.com
                  </a>
                </Paragraph>
              </div>
              <EmailIlustration />
            </div>
          )}
          <div className="footer__form-wrap">
            <Formik
              initialValues={{
                fullName: "",
                email: "",
                message: "",
              }}
              validationSchema={contactSchema}
              onSubmit={handleSubmit}
            >
              <Form className="footer__form">
                <div className="footer__field-group">
                  <Input
                    variant={"light"}
                    name="fullName"
                    label="Ime i prezime *"
                    type="text"
                  />
                  <Input
                    variant={"light"}
                    label="Email *"
                    name="email"
                    type="email"
                  />
                </div>
                <Input
                  variant={"light"}
                  label="Poruka *"
                  name="message"
                  type="text"
                />
                <Button
                  variant={"secondary"}
                  type="submit"
                  isLoading={isLoading}
                >
                  Pošalji poruku
                  <SendIcon />
                </Button>
              </Form>
            </Formik>
          </div>
        </section>
      )}
      <div className="footer__copyright">
        <Paragraph variant={"p3"}>
          Sva prava zadržana © 2023 NeoUm Media
        </Paragraph>
      </div>
    </footer>
  );
};

export default Footer;
