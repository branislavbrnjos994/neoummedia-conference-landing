import React from "react";
import { useField } from "formik";
import { ReactComponent as ConfirmIcon } from "../assets/svg/confirm.svg";

const Checkbox = ({ name, label, ...props }) => {
  const [field, meta, helpers] = useField(name);

  const handleCheckboxChange = () => {
    helpers.setValue(!field.value);
  };

  return (
    <div
      className={`checkbox ${field.value ? "checkbox--checked" : ""} ${
        meta.error && meta.touched ? "checkbox--error" : ""
      }`}
    >
      <label className="checkbox_inner">
        <input
          checked={!!field.value}
          className="checkbox_field"
          type="checkbox"
          onChange={handleCheckboxChange}
          onBlur={() => helpers.setTouched(true)}
          {...field}
          {...props}
        />

        <div className="checkbox_indicator">
          <ConfirmIcon />
        </div>

        {label && <p className="checkbox_label">{label}</p>}
      </label>
    </div>
  );
};

export default Checkbox;
