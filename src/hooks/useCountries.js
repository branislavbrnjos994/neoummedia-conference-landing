import countries from "i18n-iso-countries";

export const useCountries = () => {
  const countriesList = countries.getNames("hr", { select: "official" });

  return { countries: countriesList };
};
